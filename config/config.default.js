'use strict';

const config = {};

config.keys = 'security';

config.logger = {
  consoleLevel: 'DEBUG',
};

config.mongoose = {
  client: {
    url: 'mongodb://127.0.0.1/ddzj',
    options: {},
  },
};
config.redis = {
  client: {
    port: 6379,
    host: '127.0.0.1',
    db: 0,
    password: '',
  },
};

module.exports = config;
