'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  app.logger.debug(app.model);
  const StickerSchema = new Schema({
    name: { type: String },
    student: new Schema({ name: 'string' }),
  });

  return mongoose.model('Sticker', StickerSchema, 'sticker');
};
