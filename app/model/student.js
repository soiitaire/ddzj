'use strict';

module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const StudentSchema = new Schema({
    name: { type: String },
  });

  return mongoose.model('Student', StudentSchema, 'student');
};
