'use strict';
const Service = require('egg').Service;

class StickerService extends Service {
  async create(payload) {
    return this.ctx.model.Sticker.create(payload);
  }
  async find() {
    /*
    const student = await this.ctx.model.Student.create({ name: '李雷' });
    this.logger.debug(student);
    await this.ctx.model.Sticker.create({
      name: '维尼熊',
      student,
    });
    */
    return await this.ctx.model.Sticker.find({});
  }
}

module.exports = StickerService;
