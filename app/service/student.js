'use strict';
const Service = require('egg').Service;

class StudentService extends Service {
  async create(payload) {
    return this.ctx.model.Student.create(payload);
  }
  async findOne() {
    return this.ctx.model.Sticker.findOne({});
  }

  async getStickers() {
    const ObjectId = this.app.mongoose.Types.ObjectId;
    const redis = this.app.redis;
    let stickers;
    stickers = await redis.get('sticker:5d419e26d19187889183f659');
    this.logger.debug(stickers);
    if (stickers) return JSON.parse(stickers);
    stickers = await this.ctx.model.Sticker.find({ 'student._id': ObjectId('5d419e26d19187889183f659') });
    // this.logger.debug(stickers);
    await redis.set('sticker:5d419e26d19187889183f659', JSON.stringify(stickers));
    return stickers.toObject();
  }
}

module.exports = StudentService;
