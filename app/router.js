'use strict';

module.exports = app => {
  const { router, controller } = app;
  router.get('/sticker', controller.sticker.index);
  router.get('/student', controller.student.index);
};
