'use strict';
const Controller = require('egg').Controller;

class StudentController extends Controller {
  async index() {
    const { ctx, service } = this;
    const stickers = await service.student.getStickers();
    ctx.body = {
      stickers,
    };
  }
}

module.exports = StudentController;
