'use strict';
const Controller = require('egg').Controller;

class StickerController extends Controller {
  async index() {
    const { ctx, service } = this;
    const stickers = await service.sticker.find({});
    ctx.body = {
      stickers,
    };
  }
}

module.exports = StickerController;
